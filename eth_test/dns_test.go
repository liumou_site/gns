package eth_test

import (
	"gitee.com/liumou_site/gns/eth"
	"testing"
)

// TestGetEthDNS 测试获取以太网接口的DNS配置。
// 该测试首先获取默认的以太网接口，然后查询该接口的DNS配置，并验证DNS配置是否为空。
func TestGetEthDNS(t *testing.T) {
	// 获取默认的以太网接口
	defaultInterface, err := eth.GetDefaultInterface()
	if err != nil {
		// 如果获取默认接口失败，则记录错误信息并返回
		t.Errorf("failed to get default interface: %v", err)
		return
	}

	// 创建一个新的以太网接口信息对象
	info := eth.NewEth()

	// 获取默认以太网接口的DNS配置
	dns, err := info.GetEthDNS(defaultInterface.Name)
	if err != nil {
		// 如果获取DNS配置失败，则直接返回
		return
	}

	// 验证DNS配置是否为空
	if len(dns) == 0 {
		// 如果DNS配置为空，则记录错误信息并返回
		t.Errorf("DNS for interface %s: %v", defaultInterface.Name, dns)
		return
	}

	// 如果DNS配置不为空，则记录DNS配置信息
	t.Logf("DNS for interface %s: %v", defaultInterface.Name, dns)
}
