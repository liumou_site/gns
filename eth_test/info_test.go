package eth_test

import (
	eth2 "gitee.com/liumou_site/gns/eth"
	"testing"
)

// TestGetNetInterface 测试获取网络接口信息的功能。
// 该测试验证了是否能够正确获取和显示网络接口的详细信息，如IP地址、网关地址等。
func TestGetNetInterface(t *testing.T) {
	// 创建一个Eth实例
	eth := eth2.NewEth()
	info, err := eth2.GetDefaultInterface()
	if err != nil {
		t.Errorf("%s", err.Error())
		return
	}
	t.Logf("默认网卡名称: %s", info.Name)
	// 获取第一个网络接口的信息
	netInterface, err := eth.GetEthInfo(info.Name)
	if err != nil {
		// 如果发生错误，输出错误信息并结束测试
		t.Errorf("GetNetInterface error: %v", err)
		return
	}

	// 输出网络接口的信息
	t.Logf("netInterface: %v", netInterface)
	t.Logf("IP地址: %s", netInterface.Ip.String())
	t.Logf("网关地址: %s", netInterface.Gw.String())
	t.Logf("网卡地址: %s", netInterface.Mac)
	t.Logf("网段信息: %s", netInterface.Sub.String())
	t.Logf("网段掩码: %s", netInterface.Mask.String())
	t.Logf("网段掩码位数: %d", netInterface.MaskInt)
	t.Logf("DNS列表: %v", netInterface.DnsList)
	t.Logf("网卡名称: %s", netInterface.Name)
}

// TestGetEthList 测试 NewEth 实例的 GetEthList 方法是否正确返回以太坊列表。
// 该测试函数没有输入参数。
// 该测试函数没有返回值。
func TestGetEthList(t *testing.T) {
	// 创建一个 NewEth 实例。
	eth := eth2.NewEth()

	// 调用实例的 GetEthList 方法获取以太坊列表。
	e := eth.GetEthList()

	// 检查返回的以太坊列表是否为空。
	// 如果列表为空，则记录错误并返回。
	if len(e) == 0 {
		t.Errorf("GetEthList error")
		return
	}

	// 如果列表不为空，记录以太坊实例的信息。
	t.Logf("eth: %v", eth)
}

// TestGetDefaultInterface 测试获取系统默认网络接口的功能
// 该测试用例验证以下场景：
// 1. 能够成功调用 GetDefaultInterface 函数
// 2. 正确处理函数返回的错误信息
// 3. 成功获取到网络接口信息时能正确记录日志
//
// 测试步骤：
//   - 调用被测函数 GetDefaultInterface()
//   - 如果返回错误，使用 t.Errorf 记录测试失败并打印错误信息
//   - 如果成功获取，使用 t.Logf 记录获取到的网络接口信息
//
// 注意：该测试需要系统存在有效的网络连接和至少一个活动的网络接口
func TestGetDefaultInterface(t *testing.T) {
	// 获取默认网络接口信息
	defaultInterface, err := eth2.GetDefaultInterface()

	// 错误处理：如果获取接口信息失败，标记测试失败并记录错误
	if err != nil {
		t.Errorf("GetDefaultInterface error: %v", err)
		return
	}

	// 成功时记录调试信息：打印获取到的网络接口详细信息
	t.Logf("defaultInterface: %v", defaultInterface.Name)
}
