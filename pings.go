package gns

import (
	"strings"

	"gitee.com/liumou_site/logger"
	"github.com/spf13/cast"
)

// 判断IP格式
func subCheckAddress(sub string) bool {
	// 使用切片切分
	sum := strings.Split(sub, ".")
	if len(sum) != 3 {
		logger.Error("IP子网格式错误,需要传入三组整数组成的字符串,当前仅传入: ", sum)
		return false
	}
	for index, value := range sum {
		values := cast.ToInt(value)
		if values > 255 || values < 0 {
			logger.Error("子网第[ %s ]个值不符合标准: ", index)
			return false
		}
	}
	return true
}

// 开始检测处理
func (api *Info) pins(ip string, ok, failed chan int) {
	res := Ping(ip, 3, false)
	okS := <-ok
	failedS := <-failed
	if res == nil {
		logger.Debug("连接成功: ", ip)
		api.UseTxt = api.UseTxt + " " + ip
		ok <- okS + 1
	} else {
		logger.Warn("连接失败: ", ip)
		api.NotUsed = api.NotUsed + " " + ip
		failed <- failedS + 1
	}
}

// PingConcurrency 这是一个高并发的Ping检测功能
// sub 子网,例如: 10.1.1
// icmp/stop 开始及结束
// use 被使用的主机 notuse 未使用的主机(字符串返回: 10.1.1.2 10.1.1.3)可以使用切片切割数据
// res 是否进入检测条件
func PingConcurrency(sub string, start, stop int) (res bool, use, notUsed string) {
	// 开始循环
	if start >= stop {
		logger.Warn("起点大于或等于终点,请重新设置参数")
		return false, "", ""
	}
	ok := make(chan int)
	failed := make(chan int)
	sub_ := subCheckAddress(sub)
	if sub_ {
		gp := NewNet()
		gp.debug = false
		for i := start; i <= stop; i++ {
			iStr := cast.ToString(i)
			ip := sub + "." + iStr
			go gp.pins(ip, ok, failed)
		}
		logger.Info("连接成功的数量: ", ok)
		logger.Info("连接失败的数量: ", failed)
		return true, gp.UseTxt, gp.NotUsed
	}
	return false, "", ""
}
