package tools

import (
	"os/exec"

	"gitee.com/liumou_site/logger"
)

// OpenUrl 使用 xdg-open 命令在默认浏览器中打开指定的 URL。
// 参数:
//
//	url: 需要打开的网址字符串。
func OpenUrl(url string) {
	// 创建一个 *exec.Cmd 类型的变量 cmd，用于执行外部命令。
	var cmd *exec.Cmd
	// 设置 cmd 为执行 xdg-open 命令，参数为传入的 url。
	cmd = exec.Command(`xdg-open`, url)
	// 启动命令，并检查是否有错误发生。
	err := cmd.Start()
	// 如果启动命令时发生错误，则记录错误信息并返回。
	if err != nil {
		logger.Error(err.Error())
		return
	}
}
