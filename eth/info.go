package eth

import (
	"fmt"
	"gitee.com/liumou_site/logger"
	"net"
)

// GetEthListInfo 获取所有网卡的信息
// 该函数通过调用 GetEthList 和 GetEthInfo 方法获取所有网卡的信息。
// 通过 Info 字段记录所有网卡的信息。
func (e *Eth) GetEthListInfo() {
	// 获取本机所有网络接口的名称列表
	ethList := e.GetEthList()
	if len(ethList) == 0 {
		logger.Error("GetEthListInfo: 本机无网络接口")
		e.Err = fmt.Errorf("本机无网络接口")
		return
	}

	// 遍历每个网络接口进行详细信息获取
	for _, eth := range ethList {
		// 获取指定网卡的详细信息（IP地址、MAC地址、子网掩码等）
		ethInfo, err := e.GetEthInfo(eth)

		// 错误处理：当获取单网卡信息失败时
		if err != nil {
			// 记录错误日志，包含具体的错误信息
			logger.Error("GetEthListInfo: 获取网卡信息失败", err)

			// 在结构体中存储错误信息供调用方检查
			e.Err = fmt.Errorf("获取网卡[%s]信息失败: %v", eth, err)

			// 遇到错误立即终止处理流程
			return
		}

		// 将成功获取的网卡信息追加到结构体的信息集合中
		// 使用指针解引用追加结构体副本，避免内存引用问题
		e.Info = append(e.Info, *ethInfo)
	}
}

// GetEthInfo 获取指定网卡的网卡信息
func (e *Eth) GetEthInfo(eth string) (*Info, error) {
	// 获取指定网卡的详细信息
	inter, err := net.InterfaceByName(eth)
	if err != nil {
		logger.Error("GetEthInfo: 获取网卡信息失败", err)
		return nil, fmt.Errorf("获取网卡信息失败: %v", err)
	}

	// 获取指定网卡的IP地址
	adders, err := inter.Addrs()
	if err != nil {
		logger.Error("GetEthInfo: 获取IP地址失败", err)
		return nil, fmt.Errorf("获取IP地址失败: %v", err)
	}
	// 初始化EthInfo结构体
	ethInfo := &Info{
		Name:    eth,
		Ip:      nil,
		Mac:     inter.HardwareAddr.String(),
		Sub:     nil,
		Mask:    nil,
		Gw:      nil,
		DnsList: nil,
		MaskInt: 0,
	}

	// 处理IP地址
	if len(adders) == 0 {
		logger.Warn("网卡无IP地址: ", eth)
		return ethInfo, nil
	}

	for _, addr := range adders {
		// 判断是否属于IPV4
		if ip, ok := addr.(*net.IPNet); ok && !ip.IP.IsLoopback() {
			// 如果是IPV4，则将IP地址赋值给EthInfo结构体的Ip字段
			if ip.IP.To4() != nil {
				ethInfo.Ip = ip.IP
				// 将掩码赋值给EthInfo结构体的Mask字段
				ethInfo.Sub = ip.IP.Mask(ip.Mask)
				ethInfo.Gw, err = e.GetEthGateway(eth, ip.IP)
				if err != nil {
					logger.Error("GetEthInfo: 获取网关地址失败", err)
					return ethInfo, fmt.Errorf("获取网关地址失败: %v", err)
				}
				_, info, err := net.ParseCIDR(addr.String())
				if err != nil {
					logger.Error("GetEthInfo: 获取网段失败", err)
					return ethInfo, fmt.Errorf("%s", err.Error())
				}
				mask := info.Mask
				maskStr := net.IP(mask).String()
				ethInfo.Mask = net.ParseIP(maskStr)
				ethInfo.MaskInt, _ = ip.Mask.Size()
				ethInfo.DnsList, _ = e.GetEthDNS(eth)
				return ethInfo, nil
			}
		}
	}
	return ethInfo, nil
}

// GetEthList 获取当前系统中所有正在使用的网络接口名称列表。
// 该函数通过查询系统中的网络接口信息，筛选出所有处于激活状态（即接口标志中包含FlagUp）的网络接口，
// 并将其名称添加到一个字符串列表中，最后返回该列表。
// 如果在获取网络接口信息时发生错误，函数将直接返回nil。
func (e *Eth) GetEthList() []string {
	// 获取系统中的所有网络接口信息。
	interfaces, err := net.Interfaces()
	if err != nil {
		// 如果发生错误，直接返回nil。
		return nil
	}

	// 初始化一个空的字符串切片，用于存储激活状态的网络接口名称。
	var ethList []string

	// 遍历所有网络接口信息。
	for _, inter := range interfaces {
		if inter.Name == "lo" || inter.Name == "docker0" {
			logger.Debug("Ignore loopback and Docker network card: ", inter.Name)
			continue
		}
		// 检查网络接口是否处于激活状态。
		if inter.Flags&net.FlagUp != 0 {
			// 如果是，将接口名称添加到列表中。
			ethList = append(ethList, inter.Name)
		} else {
			logger.Warn("Ignore inactive or virtual network cards: ", inter.Name)
		}
	}
	// 返回包含所有激活状态网络接口名称的列表。
	return ethList
}
