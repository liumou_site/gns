package eth

import (
	"bytes"
	"fmt"
	"gitee.com/liumou_site/logger"
	"golang.org/x/text/encoding/simplifiedchinese"
	"golang.org/x/text/transform"
	"io"
	"net"
	"os"
	"os/exec"
	"runtime"
	"strings"
)

// GetEthDNS 获取指定网卡的DNS配置
func (e *Eth) GetEthDNS(eth string) ([]net.IP, error) {
	switch runtime.GOOS {
	case "windows":
		return e.getWindowsDNS(eth)
	case "linux", "darwin":
		return e.getUnixDNS()
	default:
		return nil, fmt.Errorf("unsupported OS: %s", runtime.GOOS)
	}
}

// getWindowsDNS 获取Windows系统的DNS配置
func (e *Eth) getWindowsDNS(eth string) ([]net.IP, error) {
	out, err := exec.Command("ipconfig", "/all").Output()
	if err != nil {
		logger.Error(err)
		return nil, fmt.Errorf("failed to execute ipconfig: %v", err)
	}
	// 将GBK编码转换为UTF-8
	decoder := simplifiedchinese.GBK.NewDecoder()
	reader := transform.NewReader(bytes.NewReader(out), decoder)
	utf8Out, err := io.ReadAll(reader)
	var dnsServers []net.IP
	lines := strings.Split(string(utf8Out), "\n")
	foundInterface := false
	for index, line := range lines {
		if strings.Contains(line, eth) {
			foundInterface = true
			continue
		}
		logger.Debug(index, line)
		if foundInterface && strings.HasPrefix(line, "  DNS") {
			parts := strings.Fields(line)
			if len(parts) > 1 {
				ip := net.ParseIP(parts[1])
				if ip != nil {
					dnsServers = append(dnsServers, ip)
				}
			}
		}
	}
	return dnsServers, nil
}

// getUnixDNS 获取Unix系统的DNS配置
func (e *Eth) getUnixDNS() ([]net.IP, error) {
	out, err := os.ReadFile("/etc/resolv.conf")
	if err != nil {
		return nil, fmt.Errorf("failed to read /etc/resolv.conf: %v", err)
	}

	var dnsServers []net.IP
	lines := strings.Split(string(out), "\n")
	for _, line := range lines {
		if strings.HasPrefix(line, "nameserver") {
			parts := strings.Fields(line)
			if len(parts) > 1 {
				ip := net.ParseIP(parts[1])
				if ip != nil {
					dnsServers = append(dnsServers, ip)
				}
			}
		}
	}
	return dnsServers, nil
}
