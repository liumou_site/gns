package eth

import "net"

// Eth 结构体用于封装网卡信息和错误信息。
// 它包含两个字段：网卡信息列表和错误信息。
type Eth struct {
	Info []Info `json:"info"` // Info 字段是一个 Info 类型的切片，用于存储网卡信息列表。
	Err  error  `json:"err"`  // Err 字段用于存储在获取网卡信息过程中可能发生的错误。
}

type Info struct {
	Name    string   `json:"name" `   // 网卡名称,例如 eth0
	Mac     string   `json:"mac"`     // 网卡mac,例如 00:00:00:00:00:00
	Ip      net.IP   `json:"ip"`      // 网卡ip, 例如 192.168.1.1
	Gw      net.IP   `json:"gw"`      // 网关, 例如 192.168.1.1
	Sub     net.IP   `json:"sub"`     // 网段, 例如 192.168.1.0
	Mask    net.IP   `json:"mask"`    // 网卡掩码, 例如 255.255.255.0
	MaskInt int      `json:"maskInt"` // 网卡掩码, 例如 24
	DnsList []net.IP `json:"dnsList"` // DNS列表
}

// NewEth 创建并返回一个新的 Eth 实例。
// 该函数不接受任何参数。
// 返回值是一个指向 Eth 类型的指针，表示新创建的 Eth 实例。
func NewEth() *Eth {
	eth := new(Eth)
	eth.GetEthList()
	eth.GetEthListInfo()
	return eth
}
