package eth

import (
	"fmt"
	"gitee.com/liumou_site/logger"
	"net"
	"os/exec"
	"regexp"
	"runtime"
	"strings"
)

// GetEthGateway 根据指定的以太网接口和IP地址获取网关地址。
// 参数:
//
//	eth: 以太网接口名称，例如 "eth0"。
//	ip: 网络接口的IP地址。
//
// 返回值:
//
//	net.IP: 网关的IP地址。
//	error: 如果获取网关地址时发生错误，则返回该错误。
func (e *Eth) GetEthGateway(eth string, ip net.IP) (net.IP, error) {
	// 根据操作系统的不同，使用不同的命令获取网关信息。
	var cmd string
	var out []byte
	var err error
	var gw net.IP
	if len(eth) == 0 {
		// 如果未提供以太网接口名称，返回错误。
		return nil, fmt.Errorf("interface name is required")
	}
	// 根据操作系统类型选择合适的命令执行。
	switch runtime.GOOS {
	case "windows":
		// 在Windows系统上，使用route命令获取网关信息。
		cmd = fmt.Sprintf("route print -4 | findstr %s", ip.String())
		out, err = exec.Command("cmd", "/c", cmd).Output()
	case "darwin":
		// 在macOS系统上，使用netstat命令获取网关信息。
		cmd = fmt.Sprintf("netstat -nr | grep %s | awk '{print $2}'", eth)
		out, err = exec.Command("bash", "-c", cmd).Output()
	default:
		// 在Linux系统上，使用ip命令获取网关信息。
		cmd = fmt.Sprintf("ip route show dev %s | awk '{print $3}'", eth)
		out, err = exec.Command("bash", "-c", cmd).Output()
	}

	// 如果命令执行出错，返回错误。
	if err != nil {
		logger.Error(err)
		return nil, err
	}

	// 根据操作系统类型解析命令输出以获取网关地址。
	if strings.EqualFold(runtime.GOOS, "windows") {
		// 在Windows系统上，使用正则表达式解析网关地址。
		gateway := regexp.MustCompile(`0.0.0.0\s+0.0.0.0\s+(\d+\.\d+\.\d+\.\d+)`).FindStringSubmatch(string(out))
		if len(gateway) > 1 {
			gw = net.ParseIP(gateway[1])
			if gw == nil {
				err = fmt.Errorf("invalid gateway address: %s", gateway[1])
				logger.Error(err)
				return nil, err
			}
			// 返回网关地址
			return gw, nil
		}
		// 如果未找到网关地址，返回空网关地址和nil错误。
		logger.Error("未找到网关地址....")
		return nil, fmt.Errorf("invalid gateway address: %s", gateway[1])
	}

	// 在非Windows系统上，根据系统类型解析网关地址。
	if strings.EqualFold(runtime.GOOS, "darwin") {
		// 在macOS系统上，使用正则表达式解析网关地址。
		gateway := regexp.MustCompile(`(\d+\.\d+\.\d+\.\d+)`).FindStringSubmatch(string(out))
		if len(gateway) > 1 {
			gw = net.ParseIP(gateway[1])
			if gw == nil {
				// 如果解析失败，返回错误。
				return nil, fmt.Errorf("invalid gateway address: %s", gateway[1])
			}
			return gw, nil
		}
		return nil, fmt.Errorf("invalid gateway address: %s", gateway[1])
	}
	// 在Linux系统上，调用专门的函数解析网关地址。
	gateway, err := linuxGetGateway(eth)
	if err != nil {
		logger.Error(err.Error())
		return nil, err
	}
	return gateway, nil
}

// linuxGetGateway 获取Linux系统中指定网卡的网关地址。
// 参数:
//
//	eth: 网卡名称，用于指定要查询网关的网络接口。
//
// 返回值:
//
//	net.IP: 网关的IP地址。
//	error: 错误信息，如果执行命令失败或解析IP地址失败，则返回错误。
func linuxGetGateway(eth string) (net.IP, error) {
	// 构建命令以获取指定网卡的网关地址。
	cmd := fmt.Sprintf("ip route show dev %s | sed -n 1p | awk '{print $3}'", eth)

	// 执行命令并获取输出。
	out, err := exec.Command("sh", "-c", cmd).Output()
	if err != nil {
		// 如果执行命令失败，记录错误信息并返回。
		logger.Error("linuxGetGateway: ", err)
		return nil, err
	}
	out = []byte(strings.TrimSpace(string(out)))
	// 解析输出，将其转换为IP地址并返回。
	return net.ParseIP(string(out)), nil
}

// GetDefaultInterface 获取当前系统用于连接默认路由的网络接口
// 该方法通过创建一个虚拟的UDP连接（目标为公共DNS服务器），利用系统路由表确定默认出口网卡
// 返回: 成功返回网络接口指针，失败返回错误信息
func GetDefaultInterface() (*net.Interface, error) {
	// 创建虚拟UDP连接（使用Google公共DNS）
	// 系统会自动选择默认路由的网卡建立连接，通过此连接可反向查询使用的本地地址
	conn, err := net.Dial("udp", "8.8.8.8:53")
	if err != nil {
		return nil, fmt.Errorf("创建探测连接失败: %v", err)
	}
	// 确保连接关闭（延迟执行）
	defer func(conn net.Conn) {
		err = conn.Close()
		if err != nil {
			logger.Error("关闭连接失败: %v", err)
		}
	}(conn)

	// 获取连接使用的本地地址信息（类型断言为UDP地址）
	localAddr := conn.LocalAddr().(*net.UDPAddr)

	// 获取系统所有网络接口列表
	ifNameList, err := net.Interfaces()
	if err != nil {
		logger.Error("获取本地接口失败: %v", err)
		return nil, err
	}
	if len(ifNameList) == 0 {
		return nil, fmt.Errorf("未找到任何网络接口")
	}
	// 遍历所有网络接口，寻找与本地地址匹配的接口
	for _, iface := range ifNameList {
		// 获取接口绑定的所有IP地址
		adders, err := iface.Addrs()
		if err != nil {
			continue // 跳过无法获取地址的接口（可能是权限问题或无效接口）
		}

		// 检查每个地址是否匹配目标IP
		for _, addr := range adders {
			// 类型断言为IPNet，并排除回环地址
			if ipnet, ok := addr.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
				// 比较接口地址与连接使用的本地地址
				if ipnet.IP.Equal(localAddr.IP) {
					return &iface, nil // 找到匹配的接口
				}
			}
		}
	}

	// 未找到匹配的网卡（可能系统没有默认路由或网络配置异常）
	return nil, fmt.Errorf("未找到默认网卡")
}
