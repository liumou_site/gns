import os
from sys import exit

from loguru import logger
from gitee import gitee

version = input("请输入版本号,例如: 1.1.0\n")
mess = input("请输入版本描述\n")
# 判断是否输入有效
while version == "":
	logger.error("版本号不能为空")
	version = input("请输入版本号,例如: 1.1.0\n")
while mess == "":
	logger.error("版本描述不能为空")
	mess = input("请输入版本描述\n")
versionV = f"v{version}"

logger.debug(f"版本号: {version}")
logger.debug(f"版本描述: {mess}")
# 添加所有文件名称带 "go"的文件进行提交
try:
	os.system("git add *go*")
	os.system("git commit -m '" + mess + "'")
except Exception as e:
	logger.error(e)
	exit(1)
# 打标签
try:
	os.system("git tag -a " + versionV + " -m '" + mess + "'")
except Exception as e:
	logger.error(e)
	exit(2)

try:
	os.system("git push origin " + versionV)
except Exception as e:
	logger.error(e)
	exit(3)
gitee(ver=version, message=mess, prerelease=False)
logger.info("发布成功")
