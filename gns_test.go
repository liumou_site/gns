package gns

import (
	"testing"
	"time"
)

// TestPortTcpStatus 测试端口TCP状态
// 该函数尝试连接指定的主机和端口，并根据连接结果记录日志。
// 参数:
//   - host: 要测试的主机地址
//   - port: 要测试的端口号
//   - Timeout: 连接超时时间（秒）
func TestPortTcpStatus(t *testing.T) {
	// 测试baidu.com的444端口
	gets := PortTcpStatus("baidu.com", "444", 3)
	if gets {
		t.Error("访问成功")
	} else {
		t.Log("访问失败")
	}
	// 测试baidu.com的443端口
	get := PortTcpStatus("baidu.com", "443", 3)
	if get {
		t.Log("访问成功")
	} else {
		t.Error("访问失败")
	}
}

// 检测UDP端口是否开启(目前并不准确)
// TestPortUdpStatus 测试通过UDP协议检查指定主机和端口的连通性
// 该函数旨在验证PortUdpStatus函数的行为是否符合预期
func TestPortUdpStatus(t *testing.T) {
	// 检查百度的HTTPS端口，预期会失败因为HTTPS通常不通过UDP
	gets := PortUdpStatus("baidu.com", 443, 3*time.Second)
	if gets == nil {
		t.Log("访问成功")
	} else {
		t.Error("访问失败")
	}

	// 检查阿里云DNS服务器的DNS端口，DNS服务通常通过UDP
	g := PortUdpStatus("223.5.5.5", 53, 3*time.Second)
	if g == nil {
		t.Log("访问成功")
	} else {
		t.Error("访问失败")
	}
	// 检查阿里云DNS服务器的DNS端口，DNS服务通常通过UDP
	g = PortUdpStatus("223.5.5.5", 666, 3*time.Second)
	if g == nil {
		t.Error("访问成功")
	} else {
		t.Log("访问失败")
	}
}

func TestHttpUrl(t *testing.T) {
	p := NewNet()
	p.https = true
	get := p.HttpStatus("liumou.site")
	if get {
		t.Log("访问成功")
	} else {
		t.Error("访问失败")
	}
}

// TestHttpGetStatus 测试 HTTP 状态码获取功能。
//
// 该测试函数旨在验证 HttpStatusUrl 方法能否正确返回指定 URL 的 HTTP 状态码。
//
// 参数: t *testing.T 用于报告测试结果中的错误信息。
func TestHttpGetStatus(t *testing.T) {
	// 创建 Net 实例以使用其 HttpStatusUrl 方法。
	p := NewNet()

	// 对 "https://baidu.com/" 进行 HTTP 状态码检查。
	get := p.HttpStatusUrl("https://baidu.com/", 3*time.Second)
	if get == nil {
		t.Log("与预期一致")
	} else {
		t.Error("与预期不一致")
	}

	// 对 "https://baidu.com/aa" 进行 HTTP 状态码检查。
	p.code = 404
	get = p.HttpStatusUrl("https://baidu.com/aa", 3*time.Second)
	if get == nil {
		t.Log("与预期一致")
	} else {
		t.Error("与预期不一致")
	}
	// 对 "https://baidu.com/aa" 进行 HTTP 状态码检查。
	p.code = 200
	get = p.HttpStatusUrl("https://baidu.com/aa", 3*time.Second)
	if get == nil {
		t.Errorf("与预期不一致")
	} else {
		t.Log("与预期一致")
	}
}
