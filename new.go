package gns

import (
	"net/http"
)

// NewNet 初始化配置
func NewNet() *Info {
	g := new(Info)
	g.debug = false
	g.agreement = "TCP"
	g.https = false
	g.port = 80
	g.request = "GET"
	g.Timeout = 5
	g.code = 200
	g.pingCount = 5
	g.Client = &http.Client{}
	return g
}

func NewPing(count, timeout int) *PingCmd {
	p := new(PingCmd)
	p.pingCount = count
	p.timeout = timeout
	p.Realtime = false
	p.ExitStatus = false
	return p
}
