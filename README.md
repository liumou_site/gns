# gns

## 介绍

使用Go编写的网络模块

### 功能清单


#### 状态检测

* TCP/UDP端口检测
* 网站/网址状态检测(可自定义状态码)
* `PING`检测
* `批量PING`检测
  
#### 信息获取

* 本机IP地址获取
* 本机互联网出口地址获取

#### 其他

* 文件下载(只建议下载小文件,如需下载大文件请使用gdf项目)
  
## 使用说明

### 安装

```shell
go get -u gitee.com/liumou_site/gns
```

### 使用方法

直接参照单元测试Demo

[状态检测Demo](gns_test.go)

[信息获取Demo](ip_test.go)

# 问题反馈

点击链接加入QQ群聊【[坐公交也用券](https://jq.qq.com/?_wv=1027&k=FEeLQ6tz)】