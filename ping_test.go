package gns

import (
	"testing"
)

func TestPingLoss(t *testing.T) {
	p := NewPing(3, 3)
	err, i := p.PingLoss("baidu.com", 4)
	if err != nil {
		t.Error(err)
		return
	}
	t.Logf("丢包率: %d", i)
	err, i = p.PingLoss("ssh.liumou.site", 4)
	if err != nil {
		t.Logf(err.Error())
		return
	}
	if i == 0 {
		t.Errorf("丢包率: %d", i)
	} else {
		t.Logf("丢包率: %d", i)
	}
}
func TestPing(t *testing.T) {
	get := Ping("baidu.com", 5, true)
	if get == nil {
		t.Log("Ping成功")
	} else {
		t.Error("Ping失败")
	}
	get = Ping("baidu.com", 5, false) // 关闭信息打印
	if get == nil {
		t.Log("Ping成功")
	} else {
		t.Error("Ping失败")
	}
}

func TestSysPing(t *testing.T) {
	p := NewPing(5, 5)
	get := p.PingCmd("baidu.com")
	if get == nil {
		t.Log("Ping成功")
	} else {
		t.Errorf("Ping失败: %s", get.Error())
	}
	p.Realtime = true
	get = p.PingCmd("baidusss.csom") // 关闭信息打印
	if get == nil {
		t.Error("Ping成功")
	} else {
		t.Logf("Ping失败: %s", get.Error())
	}
}

// 批量ping
func TestPingS(t *testing.T) {
	s, _, _ := PingConcurrency("10.16.17", 1, 2)
	if s {
		t.Log("符合测试要求")
	} else {
		t.Error("不符合测试要求")
	}
}
