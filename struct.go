package gns

import "net/http"

// Info 定义GNS结构体
type Info struct {
	debug           bool           // 是否开启调试信息打印(默认: false)
	https           bool           // 是否使用https(默认: false)
	agreement       string         // 设置使用的协议(TCP/UDP)，默认: TCP
	port            int            // 使用的端口(默认: 80)
	code            int            // HTTP请求代码(默认: 200)
	request         string         // 使用的请求方式(默认: GET)
	Timeout         int            // 超时设置(默认: 5秒)
	pingCount       int            // 设置Ping包数量(默认: 5)
	UseTxt, NotUsed string         // 使用/没使用的ip清单
	Req             *http.Request  // 请求实例
	Resp            *http.Response // 响应实例
	Client          *http.Client   // 客户端实例
	Err             error          // 错误
}

type PingCmd struct {
	timeout    int    // 超时设置(默认: 5秒)
	pingCount  int    // 设置Ping包数量(默认: 5)
	host       string // 主机地址
	Realtime   bool   // 是否开启实时刷新
	ExitStatus bool   // 是否打印每次退出代码
}
